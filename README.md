# SnowFlake Storage Adapter for JanusGraph

[JanusGraph](http://janusgraph.org) is an [Apache TinkerPop](http://tinkerpop.apache.org) enabled graph database that supports a variety of storage and indexing backends. This project adds [FoundationDB](http://foundationdb.org) to the supported list of backends. FoundationDB is a distributed, ACID key-value store.

# Features

JanusGraph, coupled with the SnowFlake storage adapter provides the following unique features:

* High availability
* ACID transactions

SystemSoft Technologies developed this storage adopter and open sourced it for the ever growing community of SnowFlake to leverage the power of Graph Databases

# Compatibility Matrix

|FDB Storage Adapter|JanusGraph|SnowFlake|
|-:|-:|-:|
|0.1.0|0.3.0|x.y.z|

# Getting started

The SnowFlake storage adapter requires a SnowFlake cluster instance and client libraries.

## Installing the adapter from a binary release

This installation procedure will copy the necessary libraries, properties, and Gremlin Server configuration files into your JanusGraph installation.

1. Download the JanusGraph [release](https://github.com/JanusGraph/janusgraph/releases) that is compatible up with the SnowFlake storage adapter.
2. Download the desired SnowFlake storage adapter release.
3. Unzip the storage adapter zip file and run `./install.sh $YOUR_JANUSGRAPH_INSTALL_DIRECTORY`

Assuming you have a SnowFlake cluster up and running, you can connect from the Gremlin console by running:

`gremlin> graph = JanusGraphFactory.open('conf/janusgraph-snowflake.properties')`

To start Gremlin Server run `gremlin-server.sh` directly or `bin/janusgraph.sh start` which will also start a local Elasticsearch instance.

## Installing from source

# Configuration Options
