package com.sstech.janusgraph.diskstorage.snowflake.utils.connection;

public class SnowFlakeConnectionException extends Exception {
    public SnowFlakeConnectionException(String s)
    {
        // Call constructor of parent Exception
        super(s);
    }
}