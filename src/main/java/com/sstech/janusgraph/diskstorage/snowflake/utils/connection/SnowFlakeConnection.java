package com.sstech.janusgraph.diskstorage.snowflake.utils.connection;

import com.sstech.janusgraph.diskstorage.snowflake.utils.OperationStatus;
import com.sstech.janusgraph.diskstorage.snowflake.utils.snowflakedb.SnowFlakeCreateStatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class SnowFlakeConnection {

    private String URL = "";
    private Integer port = 0;
    private String warehouse = "";
    private String extra = "";

    private String connectionURL = "";
    private Connection snowflakeJdbcConnection;
    private SnowFlakeCreateStatement createStatement;

    private boolean autoCommit = false;

    public SnowFlakeConnection(boolean auto_commit) {
        autoCommit = auto_commit;
    }

    public void setURL(String path) {
        URL = path;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setPort(Integer portNumber) {
        port = portNumber;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setWarehouse(String warehouse_name) {
        warehouse = warehouse_name;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setOthers(String others) {
        extra = others;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    private boolean areParametersSet() {
        if (URL.equals("") && port != 0 && warehouse.equals("")) {
            return true;
        }
        else {
            return false;
        }
    }

    private void connect() throws SQLException {
        // build connection properties
        Properties properties = new Properties();
        properties.put("user", "");        // replace "" with your user name
        properties.put("password", "");    // replace "" with your password
        properties.put("warehouse", "");   // replace "" with target warehouse name
        properties.put("db", "");          // replace "" with target database name
        properties.put("schema", "");      // replace "" with target schema name
        //properties.put("tracing", "on"); // optional tracing property

        // Replace <account> with your account, as provided by Snowflake.
        // Replace <region_id> with the name of the region where your account is located.
        // If your platform is AWS and your region ID is US West, you can omit the region ID segment.
        // Replace <platform> with your platform, for example "azure".
        // If your platform is AWS, you may omit the platform.
        // Note that if you omit the region ID or the platform, you should also omit the
        // corresponding "."  E.g. if your platform is AWS and your region is US West, then your
        // connectStr will look similar to:
        // "jdbc:snowflake://xy12345.snowflakecomputing.com";
        String connectStr = "jdbc:snowflake://<account>.<region_id>.<platform>.snowflakecomputing.com";
        connectStr = connectionURL;
        snowflakeJdbcConnection = DriverManager.getConnection(connectStr, properties);
    }

    private void buildConnectionURL() {
        connectionURL = URL + "Here we build connection URL" + port.toString() + warehouse;
    }

    private void buildConnection() throws SQLException {
        this.connect();
        createStatement = new SnowFlakeCreateStatement(snowflakeJdbcConnection);
    }

    public SnowFlakeConnection getConnection() throws SQLException {
        this.buildConnection();
        return this;
    }

    public OperationStatus executeUpdate(String sql) {
        try {
            OperationStatus status = new OperationStatus(true);
            status.setResult(createStatement.executeUpdate(sql));
            return status;
        }
        catch (SQLException e) {
            return  new OperationStatus(false);
        }
    }

    public OperationStatus executeQuery(String sql) {
        try {
            OperationStatus status = new OperationStatus(true);
            status.setResult(createStatement.executeQuery(sql));
            return status;
        }
        catch (SQLException e) {
            return  new OperationStatus(false);
        }
    }

    public OperationStatus close() {
        try {
            createStatement.close();
            snowflakeJdbcConnection.close();
            return new OperationStatus(true);
        }
        catch (SQLException e) {
            return new OperationStatus(false);
        }
    }
}
