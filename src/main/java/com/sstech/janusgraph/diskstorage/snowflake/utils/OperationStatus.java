package com.sstech.janusgraph.diskstorage.snowflake.utils;

public class OperationStatus {
    public boolean SUCCESS = true;
    public boolean FAILED = false;

    public boolean KEYEXIST = true;

    private boolean operationStatus;
    private Object resultSet;

    public OperationStatus(boolean status) {
        operationStatus = status;
    }

    public void setResult(Object result) {
        resultSet = result;
    }

    public Object getResult() {
        return resultSet;
    }

    public boolean getStatus() {
        return operationStatus;
    }

}
