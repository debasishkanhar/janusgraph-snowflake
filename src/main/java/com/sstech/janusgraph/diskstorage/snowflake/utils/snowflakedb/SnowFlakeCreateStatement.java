package com.sstech.janusgraph.diskstorage.snowflake.utils.snowflakedb;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SnowFlakeCreateStatement {

    private Connection connection;
    private Statement statement;

    public SnowFlakeCreateStatement(Connection snowFlakeConnection) throws SQLException {
        connection = snowFlakeConnection;
        statement = connection.createStatement();
    }

    public int executeUpdate(String sql) throws SQLException {
        return statement.executeUpdate(sql);
    }

    public ResultSet executeQuery(String sql) throws SQLException {
        return statement.executeQuery(sql);
    }

    public void close() throws SQLException {
        statement.close();
        connection.close();
    }

}
