package com.sstech.janusgraph.diskstorage.snowflake.utils.connection;

import java.sql.SQLException;

public class SnowFlakeConnectionManager {

    private Integer commitTime;
    private Long startTime;
    private String URL = "";
    private Integer port = 0;
    private String warehouse = "";
    private String extra = "";
    private SnowFlakeConnection connection;
    private boolean isConnected = false;


    public SnowFlakeConnectionManager (Integer commit_time) {
        commitTime = commit_time;
        startTime = System.currentTimeMillis();
    }

    protected void autoTimer() {

    }

    public void setURL(String path) throws SQLException {
        URL = path;

        if (areParametersSet()) {
            connect();
        }
    }
    public void setPort(Integer portNumber) throws SQLException {
        port = portNumber;

        if (areParametersSet()) {
            connect();
        }
    }
    public void setWarehouse(String warehouse_name) throws SQLException {
        warehouse = warehouse_name;

        if (areParametersSet()) {
            connect();
        }
    }
    public void setOthers(String others) throws SQLException {
        extra = others;

        if (areParametersSet()) {
            connect();
        }
    }

    public SnowFlakeConnection getConnection() throws SnowFlakeConnectionException, SQLException {
        if (isConnected) {
            return connection;
        }
        else {
            throw new SnowFlakeConnectionException("All the required connection parameters " +
                    "(URL, port, warehouse) are not set before fetching connection");
        }
    }

    private boolean areParametersSet() {
        if (URL.equals("") && port != 0 && warehouse.equals("")) {
            return true;
        }
        else {
            return false;
        }
    }

    private void connect() throws SQLException  {
        SnowFlakeConnection snowFlakeConnection = new SnowFlakeConnection(false);

        snowFlakeConnection.setURL(URL);
        snowFlakeConnection.setPort(port);
        snowFlakeConnection.setWarehouse(warehouse);
        snowFlakeConnection.setOthers(extra);
        connection = snowFlakeConnection.getConnection();
    }


}
