// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



package com.sstech.janusgraph.diskstorage.snowflake;

import com.sstech.janusgraph.diskstorage.snowflake.utils.connection.SnowFlakeConnection;
import com.sstech.janusgraph.diskstorage.snowflake.utils.connection.SnowFlakeConnectionException;
import com.sstech.janusgraph.diskstorage.snowflake.utils.connection.SnowFlakeConnectionManager;
import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.common.AbstractStoreTransaction;

import java.sql.SQLException;


public class SnowFlakeTransaction  extends AbstractStoreTransaction {

    private final SnowFlakeStoreManager storeManager;
    private SnowFlakeConnectionManager connectionManager = new SnowFlakeConnectionManager(3000);

    public SnowFlakeTransaction(final BaseTransactionConfig config, SnowFlakeStoreManager manager) {
        super(config);
        this.storeManager = manager;

    }

    public SnowFlakeConnection getConnection() throws SQLException, SnowFlakeConnectionException {
        return connectionManager.getConnection();
    }

}
