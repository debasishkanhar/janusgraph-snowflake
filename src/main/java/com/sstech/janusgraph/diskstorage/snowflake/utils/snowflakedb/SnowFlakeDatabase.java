// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package com.sstech.janusgraph.diskstorage.snowflake.utils.snowflakedb;

// This is the helper class to provide a easy to use helper reference for doing SnowFlake based functionalities


import com.sstech.janusgraph.diskstorage.snowflake.utils.OperationStatus;
import com.sstech.janusgraph.diskstorage.snowflake.utils.connection.SnowFlakeConnection;
import com.sstech.janusgraph.diskstorage.snowflake.utils.connection.SnowFlakeConnectionException;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;

import java.sql.SQLException;

public class SnowFlakeDatabase {
    private String tableName;
    private SnowFlakeTransaction transaction;
    private SnowFlakeConnection connection;

    public  SnowFlakeDatabase(String name, SnowFlakeTransaction tx) throws SQLException, SnowFlakeConnectionException {
        transaction = tx;
        tableName = name;
        connection = transaction.getConnection();
    }

    public OperationStatus put(StaticBuffer key, StaticBuffer value) {
        // This currently is generic put, without checking if entry exists in DB. Need to implement
        // putNoOverWrite which shall insert only when data is absent else throw error
        String statement = "INSERT INTO " + tableName + " (KEY, VALUE) VALUES " + key + value;
        return connection.executeUpdate(statement);
    }

    public OperationStatus delete(StaticBuffer key) {
        // This currently is generic put, without checking if entry exists in DB. Need to implement
        // putNoOverWrite which shall insert only when data is absent else throw error
        String statement = "TRUNCATE * FROM " + tableName + " WHERE KEY = " + key;
        return connection.executeUpdate(statement);
    }

    public OperationStatus get(StaticBuffer key) {
        String statement = "";
        return connection.executeQuery(statement);
    }

    public void close() throws PermanentBackendException {
        OperationStatus status = connection.close();
        if (!status.getStatus()) {
            throw new PermanentBackendException("Could not close connection to Graph: " + status);
        }
    }
}
