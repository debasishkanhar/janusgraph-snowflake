// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package com.sstech.janusgraph.diskstorage.snowflake;


import com.sstech.janusgraph.diskstorage.snowflake.utils.OperationStatus;
import com.sstech.janusgraph.diskstorage.snowflake.utils.connection.SnowFlakeConnectionException;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KVQuery;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeySelector;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.OrderedKeyValueStore;
import org.janusgraph.diskstorage.keycolumnvalue.StoreTransaction;
import org.janusgraph.diskstorage.BackendException;
import com.sstech.janusgraph.diskstorage.snowflake.utils.snowflakedb.SnowFlakeDatabase;
import org.janusgraph.diskstorage.util.RecordIterator;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class SnowFlakeKeyValueStore implements OrderedKeyValueStore{

    private static final Logger log = LoggerFactory.getLogger(SnowFlakeKeyValueStore.class);

    private final String name;
    private final SnowFlakeStoreManager storeManager;
    private SnowFlakeDatabase db;
    private boolean isOpen;

    public SnowFlakeKeyValueStore(String tableName, SnowFlakeStoreManager manager) throws SQLException, SnowFlakeConnectionException {
        name = tableName;
        storeManager = manager;
        this.initializeDatabase();
    }

    private void initializeDatabase() throws SQLException, SnowFlakeConnectionException {
        SnowFlakeTransaction tx =(SnowFlakeTransaction) this.storeManager.getTransaction();

        db = new SnowFlakeDatabase(name, tx);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void insert(StaticBuffer key, StaticBuffer value, StoreTransaction txh) throws BackendException {
        insert(key, value, txh, true);
    }

    public void insert(StaticBuffer key, StaticBuffer value, StoreTransaction txh, boolean allowOverwrite) throws BackendException {

        try {
            OperationStatus status;

            log.trace("db={}, op=insert, tx={}", name, txh);

            if (allowOverwrite)
                status = db.put(key, value);
            else
//                status = db.putNoOverwrite(key, value);
                status = db.put(key, value);

            if (!status.getStatus()) {
                if (status.KEYEXIST) {
                    throw new PermanentBackendException("Key already exists on no-overwrite.");
                } else {
                    throw new PermanentBackendException("Could not write entity, return status: " + status);
                }
            }
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
    }

    @Override
    public RecordIterator<KeyValueEntry> getSlice(KVQuery query, StoreTransaction txh) throws BackendException {
        log.trace("beginning db={}, op=getSlice, tx={}", name, txh);
//        final Transaction tx = getTransaction(txh);
        final StaticBuffer keyStart = query.getStart();
        final StaticBuffer keyEnd = query.getEnd();
        final KeySelector selector = query.getKeySelector();
        final List<KeyValueEntry> result = new ArrayList<>();
//        final DatabaseEntry foundKey = keyStart.as(ENTRY_FACTORY);
        final byte[] foundKey = null;
//        final DatabaseEntry foundData = new DatabaseEntry();
        final byte[] foundData = null;

//        try (final Cursor cursor = db.openCursor(tx, null)) {
//            OperationStatus status = cursor.getSearchKeyRange(foundKey, foundData, getLockMode(txh));
//            //Iterate until given condition is satisfied or end of records
//            while (status.getStatus()) {
//                StaticBuffer key = getBuffer(foundKey);
//
//                if (key.compareTo(keyEnd) >= 0)
//                    break;
//
//                if (selector.include(key)) {
//                    result.add(new KeyValueEntry(key, getBuffer(foundData)));
//                }
//
//                if (selector.reachedLimit())
//                    break;
//
//                status = cursor.getNext(foundKey, foundData, getLockMode(txh));
//            }
//        } catch (Exception e) {
//            throw new PermanentBackendException(e);
//        }
        OperationStatus status = new OperationStatus(true);
        //Iterate until given condition is satisfied or end of records
        while (status.getStatus()) {
            StaticBuffer key = getBuffer(foundKey);

            if (key.compareTo(keyEnd) >= 0)
                break;

            if (selector.include(key)) {
                result.add(new KeyValueEntry(key, getBuffer(foundData)));
            }

            if (selector.reachedLimit())
                break;

//            status = cursor.getNext(foundKey, foundData, getLockMode(txh));
        }

        log.trace("db={}, op=getSlice, tx={}, resultcount={}", name, txh, result.size());

        return new RecordIterator<KeyValueEntry>() {
            private final Iterator<KeyValueEntry> entries = result.iterator();

            @Override
            public boolean hasNext() {
                return entries.hasNext();
            }

            @Override
            public KeyValueEntry next() {
                return entries.next();
            }

            @Override
            public void close() {
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public Map<KVQuery,RecordIterator<KeyValueEntry>> getSlices(List<KVQuery> queries, StoreTransaction txh) throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(StaticBuffer key, StoreTransaction txh) throws BackendException {
        log.trace("Deletion");
        OperationStatus status;
        try {
            log.trace("db={}, op=delete, tx={}", name, txh);
            status = db.delete(key);
            if (!status.getStatus()) {
                throw new PermanentBackendException("Could not remove: " + status);
            }
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
    }

    @Override
    public StaticBuffer get(StaticBuffer key, StoreTransaction txh) throws BackendException {
        try {
//            DatabaseEntry databaseKey = key.as(ENTRY_FACTORY);
//            DatabaseEntry data = new DatabaseEntry();

            log.trace("db={}, op=get, tx={}", name, txh);

            OperationStatus status = db.get(key);

            if (status.getStatus()) {
//                final byte[] entry = status.getResult();
                final byte[] entry = null;
                return getBuffer(entry);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
//        ArrayList arr = new ArrayList<Byte>(10);
//        return StaticArrayBuffer(arr, 0, 0);
    }

    @Override
    public boolean containsKey(StaticBuffer key, StoreTransaction txh) throws BackendException {
        return get(key,txh)!=null;
    }

    @Override
    public void acquireLock(StaticBuffer key, StaticBuffer expectedValue, StoreTransaction txh) throws BackendException {
        if (this.storeManager.getTransaction() == null) {
            log.warn("Attempt to acquire lock with transactions disabled");
        } //else we need no locking
    }

    @Override
    public synchronized void close() throws BackendException {
        try {
            if(isOpen) db.close();
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
//        if (isOpen) storeManager.removeDatabase(this);
        isOpen = false;
    }

    private static StaticBuffer getBuffer(byte[] entry) {
        return new StaticArrayBuffer(entry);
    }

//    private static StaticBuffer getBuffer(DatabaseEntry entry) {
//        return new StaticArrayBuffer(entry.getData(),entry.getOffset(),entry.getOffset()+entry.getSize());
//    }
}
